-- MySQL dump 10.13  Distrib 5.7.11, for Linux (x86_64)
--
-- Host: localhost    Database: granpietra
-- ------------------------------------------------------
-- Server version	5.7.11

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `a_empresa`
--

DROP TABLE IF EXISTS `a_empresa`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `a_empresa` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `texto` text COLLATE utf8_unicode_ci NOT NULL,
  `imagem_1` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `imagem_2` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `a_empresa`
--

LOCK TABLES `a_empresa` WRITE;
/*!40000 ALTER TABLE `a_empresa` DISABLE KEYS */;
INSERT INTO `a_empresa` VALUES (1,'<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed a odio tortor. Nullam vel rutrum eros. Maecenas non porttitor arcu. Nam at purus et nisl tristique iaculis. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae.</p>\r\n\r\n<h2 class=\"destaque\">Maecenas tortor risus, suscipit a euismod eu, posuere sed augue. Integer sed porta eros. Pellentesque in nisi ante.</h2>\r\n\r\n<p>Duis ac erat faucibus, commodo arcu vehicula, ornare urna. Sed in consectetur elit. Maecenas at metus a tortor semper volutpat. Nulla porttitor scelerisque porttitor. Ut leo orci, finibus eu ornare et, ullamcorper ac nunc. Mauris ornare malesuada vehicula. Quisque massa lacus, fringilla eget metus vel, congue lacinia justo. Donec eget suscipit tortor, quis elementum arcu. Mauris tellus leo, aliquet eu pulvinar id, pulvinar eget turpis. Integer eleifend viverra ex in porta. Quisque sodales bibendum tempus. Aenean vel mauris sed nunc posuere imperdiet vitae eu metus. Vestibulum a hendrerit neque. Praesent suscipit sed quam sit amet convallis.</p>\r\n\r\n<p>Morbi suscipit pretium neque, at dapibus libero ultrices eget. Sed et sem at turpis auctor convallis in at elit. Nulla malesuada dui ac sodales rhoncus. Maecenas et ultrices metus. Sed cursus dapibus tincidunt. Morbi porta arcu nisl, hendrerit laoreet risus egestas eget. Ut est ante, rhoncus id quam ut, dictum convallis orci. Phasellus quis lacinia eros, vel finibus ipsum. Duis euismod imperdiet elit ac molestie. Mauris at purus sapien. Duis eleifend tellus in ex eleifend, quis consequat mauris accumsan.</p>\r\n','img-7537_20160414143406.JPG','img-2587_20160414143406.JPG',NULL,'2016-04-14 14:34:07');
/*!40000 ALTER TABLE `a_empresa` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `banners`
--

DROP TABLE IF EXISTS `banners`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `banners` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `ordem` int(11) NOT NULL DEFAULT '0',
  `imagem` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `banners`
--

LOCK TABLES `banners` WRITE;
/*!40000 ALTER TABLE `banners` DISABLE KEYS */;
INSERT INTO `banners` VALUES (1,0,'img-7537_20160414143316.JPG','2016-04-14 14:33:16','2016-04-14 14:33:16'),(2,1,'img-2587_20160414143322.JPG','2016-04-14 14:33:23','2016-04-14 14:33:23');
/*!40000 ALTER TABLE `banners` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `contato`
--

DROP TABLE IF EXISTS `contato`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `contato` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `whatsapp` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `telefone` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `endereco` text COLLATE utf8_unicode_ci NOT NULL,
  `google_maps` text COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `contato`
--

LOCK TABLES `contato` WRITE;
/*!40000 ALTER TABLE `contato` DISABLE KEYS */;
INSERT INTO `contato` VALUES (1,'11 9.6760.5010','11 5575·5010','comercial@puglieserevestimentos.com.br','<p>Rua Durval do Nascimento Miele, 62-A</p><p>Vila Clementino - São Paulo, SP</p><p>04026.070</p>','<iframe src=\"https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d5994.533775500125!2d-46.64757028569461!3d-23.60160679951726!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x94ce5a3cec7f76c5%3A0x58c3d69c48985bdf!2sR.+Durval+do+Nascimento+Miele%2C+62+-+Sa%C3%BAde%2C+S%C3%A3o+Paulo+-+SP%2C+04026-070!5e0!3m2!1spt-BR!2sbr!4v1460396977825\" width=\"600\" height=\"450\" frameborder=\"0\" style=\"border:0\" allowfullscreen></iframe>',NULL,NULL);
/*!40000 ALTER TABLE `contato` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `contatos_recebidos`
--

DROP TABLE IF EXISTS `contatos_recebidos`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `contatos_recebidos` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `nome` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `telefone` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `mensagem` text COLLATE utf8_unicode_ci NOT NULL,
  `lido` tinyint(1) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `contatos_recebidos`
--

LOCK TABLES `contatos_recebidos` WRITE;
/*!40000 ALTER TABLE `contatos_recebidos` DISABLE KEYS */;
/*!40000 ALTER TABLE `contatos_recebidos` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `migrations`
--

DROP TABLE IF EXISTS `migrations`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `migrations` (
  `migration` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `migrations`
--

LOCK TABLES `migrations` WRITE;
/*!40000 ALTER TABLE `migrations` DISABLE KEYS */;
INSERT INTO `migrations` VALUES ('2016_02_01_000000_create_users_table',1),('2016_03_01_000000_create_contato_table',1),('2016_03_01_000000_create_contatos_recebidos_table',1),('2016_04_11_175211_create_banners_table',1),('2016_04_11_175908_create_produtos_table',1),('2016_04_11_180853_create_a_empresa_table',1),('2016_04_11_180935_create_tecnologia_table',1),('2016_04_11_225603_create_noticias_table',1),('2016_04_14_134454_create_obras_table',1),('2016_04_14_135929_create_obras_imagens_table',1);
/*!40000 ALTER TABLE `migrations` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `noticias`
--

DROP TABLE IF EXISTS `noticias`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `noticias` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `data` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `titulo` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `slug` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `capa` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `texto` text COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `noticias`
--

LOCK TABLES `noticias` WRITE;
/*!40000 ALTER TABLE `noticias` DISABLE KEYS */;
INSERT INTO `noticias` VALUES (1,'2016-04-15','Exemplo','exemplo','img-2576_20160414144229.JPG','<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed a odio tortor. Nullam vel rutrum eros. Maecenas non porttitor arcu. Nam at purus et nisl tristique iaculis. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Maecenas tortor risus, suscipit a euismod eu, posuere sed augue. Integer sed porta eros. Pellentesque in nisi ante. Phasellus sed rhoncus ex. Nulla sed risus id dolor fringilla elementum sed vel augue.</p>\r\n\r\n<h2 class=\"destaque\">Duis ac erat faucibus, commodo arcu vehicula, ornare urna.</h2>\r\n\r\n<p>Sed in consectetur elit. Maecenas at metus a tortor semper volutpat. Nulla porttitor scelerisque porttitor. Ut leo orci, finibus eu ornare et, ullamcorper ac nunc. Mauris ornare malesuada vehicula. Quisque massa lacus, fringilla eget metus vel, congue lacinia justo. Donec eget suscipit tortor, quis elementum arcu. Mauris tellus leo, aliquet eu pulvinar id, pulvinar eget turpis. Integer eleifend viverra ex in porta. Quisque sodales bibendum tempus. Aenean vel mauris sed nunc posuere imperdiet vitae eu metus. Vestibulum a hendrerit neque.</p>\r\n\r\n<h2 class=\"subtitulo\">Praesent suscipit sed quam sit amet convallis</h2>\r\n\r\n<p>Morbi suscipit pretium neque, at dapibus libero ultrices eget. Sed et sem at turpis auctor convallis in at elit. <a href=\"#\">Nulla malesuada dui</a> ac sodales rhoncus. Maecenas et ultrices metus. Sed cursus dapibus tincidunt. Morbi porta arcu nisl, hendrerit laoreet risus egestas eget. Ut est ante, rhoncus id quam ut, dictum convallis orci. Phasellus quis lacinia eros, vel finibus ipsum. Duis euismod imperdiet elit ac molestie. Mauris at purus sapien. Duis eleifend tellus in ex eleifend, quis consequat mauris accumsan.</p>\r\n','2016-04-14 14:42:31','2016-04-14 14:42:31'),(2,'2016-04-14','Exemplo 2','exemplo-2','dscn2240_20160414144300.JPG','<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed a odio tortor. Nullam vel rutrum eros. Maecenas non porttitor arcu. Nam at purus et nisl tristique iaculis. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Maecenas tortor risus, suscipit a euismod eu, posuere sed augue. Integer sed porta eros. Pellentesque in nisi ante. Phasellus sed rhoncus ex. Nulla sed risus id dolor fringilla elementum sed vel augue.</p>\r\n\r\n<h2 class=\"destaque\">Duis ac erat faucibus, commodo arcu vehicula, ornare urna.</h2>\r\n\r\n<p>Sed in consectetur elit. Maecenas at metus a tortor semper volutpat. Nulla porttitor scelerisque porttitor. Ut leo orci, finibus eu ornare et, ullamcorper ac nunc. Mauris ornare malesuada vehicula. Quisque massa lacus, fringilla eget metus vel, congue lacinia justo. Donec eget suscipit tortor, quis elementum arcu. Mauris tellus leo, aliquet eu pulvinar id, pulvinar eget turpis. Integer eleifend viverra ex in porta. Quisque sodales bibendum tempus. Aenean vel mauris sed nunc posuere imperdiet vitae eu metus. Vestibulum a hendrerit neque.</p>\r\n\r\n<h2 class=\"subtitulo\">Praesent suscipit sed quam sit amet convallis</h2>\r\n\r\n<p>Morbi suscipit pretium neque, at dapibus libero ultrices eget. Sed et sem at turpis auctor convallis in at elit. <a href=\"#\">Nulla malesuada dui</a> ac sodales rhoncus. Maecenas et ultrices metus. Sed cursus dapibus tincidunt. Morbi porta arcu nisl, hendrerit laoreet risus egestas eget. Ut est ante, rhoncus id quam ut, dictum convallis orci. Phasellus quis lacinia eros, vel finibus ipsum. Duis euismod imperdiet elit ac molestie. Mauris at purus sapien. Duis eleifend tellus in ex eleifend, quis consequat mauris accumsan.</p>\r\n','2016-04-14 14:43:02','2016-04-14 14:43:02');
/*!40000 ALTER TABLE `noticias` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `obras`
--

DROP TABLE IF EXISTS `obras`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `obras` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `ordem` int(11) NOT NULL DEFAULT '0',
  `nome` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `capa` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `obras`
--

LOCK TABLES `obras` WRITE;
/*!40000 ALTER TABLE `obras` DISABLE KEYS */;
INSERT INTO `obras` VALUES (1,0,'Exemplo','img-7537_20160414143613.JPG','2016-04-14 14:36:14','2016-04-14 14:36:14'),(2,1,'Exemplo 2','dscn2240_20160414143641.JPG','2016-04-14 14:36:42','2016-04-14 14:36:42');
/*!40000 ALTER TABLE `obras` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `obras_imagens`
--

DROP TABLE IF EXISTS `obras_imagens`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `obras_imagens` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `obra_id` int(10) unsigned NOT NULL,
  `ordem` int(11) NOT NULL DEFAULT '0',
  `imagem` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `obras_imagens_obra_id_foreign` (`obra_id`),
  CONSTRAINT `obras_imagens_obra_id_foreign` FOREIGN KEY (`obra_id`) REFERENCES `obras` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `obras_imagens`
--

LOCK TABLES `obras_imagens` WRITE;
/*!40000 ALTER TABLE `obras_imagens` DISABLE KEYS */;
INSERT INTO `obras_imagens` VALUES (1,1,0,'img-7537_20160414143620.JPG','2016-04-14 14:36:21','2016-04-14 14:36:21'),(2,1,1,'img-2576_20160414143625.JPG','2016-04-14 14:36:26','2016-04-14 14:36:26'),(3,2,0,'dscn2240_20160414143651.JPG','2016-04-14 14:36:53','2016-04-14 14:36:53'),(4,2,0,'img-2587_20160414143657.JPG','2016-04-14 14:36:58','2016-04-14 14:36:58');
/*!40000 ALTER TABLE `obras_imagens` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `produtos`
--

DROP TABLE IF EXISTS `produtos`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `produtos` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `ordem` int(11) NOT NULL DEFAULT '0',
  `nome` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `imagem` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `produtos`
--

LOCK TABLES `produtos` WRITE;
/*!40000 ALTER TABLE `produtos` DISABLE KEYS */;
INSERT INTO `produtos` VALUES (1,0,'Exemplo','produto_20160414143501.png','2016-04-14 14:35:01','2016-04-14 14:35:01'),(2,1,'Exemplo 2','produto2_20160414143549.jpg','2016-04-14 14:35:49','2016-04-14 14:35:49');
/*!40000 ALTER TABLE `produtos` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tecnologia`
--

DROP TABLE IF EXISTS `tecnologia`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tecnologia` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `texto` text COLLATE utf8_unicode_ci NOT NULL,
  `imagem_1` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `imagem_2` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tecnologia`
--

LOCK TABLES `tecnologia` WRITE;
/*!40000 ALTER TABLE `tecnologia` DISABLE KEYS */;
INSERT INTO `tecnologia` VALUES (1,'<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed a odio tortor. Nullam vel rutrum eros. Maecenas non porttitor arcu. Nam at purus et nisl tristique iaculis. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Maecenas tortor risus, suscipit a euismod eu, posuere sed augue. Integer sed porta eros. Pellentesque in nisi ante.</p>\r\n\r\n<h2 class=\"subtitulo\">PHASELLUS SED RHONCUS EX</h2>\r\n\r\n<p>Duis ac erat faucibus, commodo arcu vehicula, ornare urna. Sed in consectetur elit. Maecenas at metus a tortor semper volutpat. Nulla porttitor scelerisque porttitor. Ut leo orci, finibus eu ornare et, ullamcorper ac nunc. Mauris ornare malesuada vehicula. Quisque massa lacus, fringilla eget metus vel, congue lacinia justo. Donec eget suscipit tortor, quis elementum arcu. Mauris tellus leo, aliquet eu pulvinar id, pulvinar eget turpis. Integer eleifend viverra ex in porta. Quisque sodales bibendum tempus. Aenean vel mauris sed nunc posuere imperdiet vitae eu metus. Vestibulum a hendrerit neque. Praesent suscipit sed quam sit amet convallis.</p>\r\n','img-2576_20160414143959.JPG','img-2587_20160414144000.JPG',NULL,'2016-04-14 14:41:25');
/*!40000 ALTER TABLE `tecnologia` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `users` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(60) COLLATE utf8_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `users_email_unique` (`email`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `users`
--

LOCK TABLES `users` WRITE;
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
INSERT INTO `users` VALUES (1,'trupe','contato@trupe.net','$2y$10$IrQknjleqmAMF6GGh4lJ4.9zuA3wpv0xH08sYFb7NSDvB0fUWx6i2',NULL,NULL,NULL);
/*!40000 ALTER TABLE `users` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2016-04-14 14:43:42
