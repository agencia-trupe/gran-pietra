<?php

Route::group(['middleware' => ['web']], function () {
    Route::get('/', 'HomeController@index')->name('home');
    Route::get('a-empresa', 'AEmpresaController@index')->name('a-empresa');
    Route::get('produtos/{categoria_slug?}', 'ProdutosController@index')->name('produtos');
    Route::get('obras', 'ObrasController@index')->name('obras');
    Route::get('clientes', 'ClientesController@index')->name('clientes');
    /*
    Route::get('noticias', 'NoticiasController@index')->name('noticias');
    Route::get('noticias/{noticia_slug}', 'NoticiasController@show')->name('noticias.show');
    Route::get('tecnologia', 'TecnologiaController@index')->name('tecnologia');
    */
    Route::get('contato', 'ContatoController@index')->name('contato');
    Route::post('contato', 'ContatoController@post')->name('contato.post');

    // Painel
    Route::group([
        'prefix'     => 'painel',
        'namespace'  => 'Painel',
        'middleware' => ['auth']
    ], function() {
        Route::get('/', 'PainelController@index')->name('painel');

        /* GENERATED ROUTES */
		Route::resource('clientes', 'ClientesController');
        Route::resource('obras', 'ObrasController');
        Route::get('obras/{obras}/imagens/clear', [
            'as'   => 'painel.obras.imagens.clear',
            'uses' => 'ObrasImagensController@clear'
        ]);
		Route::resource('obras.imagens', 'ObrasImagensController');
		Route::resource('noticias', 'NoticiasController');
		Route::resource('tecnologia', 'TecnologiaController', ['only' => ['index', 'update']]);
		Route::resource('a-empresa', 'AEmpresaController', ['only' => ['index', 'update']]);
        Route::resource('produtos/categorias', 'ProdutosCategoriasController');
		Route::resource('produtos', 'ProdutosController');
		Route::resource('banners', 'BannersController');
        Route::resource('contato/recebidos', 'ContatosRecebidosController');
        Route::resource('contato', 'ContatoController');
        Route::resource('usuarios', 'UsuariosController');

        Route::post('order', 'PainelController@order');
        Route::get('logs', '\Rap2hpoutre\LaravelLogViewer\LogViewerController@index');
    });

    // Auth
    Route::group([
        'prefix'    => 'painel',
        'namespace' => 'Auth'
    ], function() {
        Route::get('login', 'AuthController@showLoginForm')->name('auth');
        Route::post('login', 'AuthController@login')->name('login');
        Route::get('logout', 'AuthController@logout')->name('logout');
    });
});
