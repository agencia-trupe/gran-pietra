<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Models\Noticia;

class NoticiasController extends Controller
{
    public function index()
    {
        $noticias = Noticia::ordenados()->get();

        return view('frontend.noticias.index', compact('noticias'));
    }

    public function show(Noticia $noticia)
    {
        $recentes = Noticia::ordenados()->where('id', '<>', $noticia->id)->take(4)->get();

        return view('frontend.noticias.show', compact('noticia', 'recentes'));
    }
}
