<?php

namespace App\Http\Controllers\Painel;

use Illuminate\Http\Request;

use App\Http\Requests\AEmpresaRequest;
use App\Http\Controllers\Controller;

use App\Models\AEmpresa;
use App\Helpers\CropImage;

class AEmpresaController extends Controller
{
    private $image_config = [
        'width'  => 800,
        'height' => null,
        'upsize' => true,
        'path'   => 'assets/img/a-empresa/'
    ];

    public function index()
    {
        $registro = AEmpresa::first();

        return view('painel.a-empresa.edit', compact('registro'));
    }

    public function update(AEmpresaRequest $request, AEmpresa $registro)
    {
        try {
            $input = array_filter($request->all(), 'strlen');
            if (isset($input['imagem_1'])) $input['imagem_1'] = CropImage::make('imagem_1', $this->image_config);
            if (isset($input['imagem_2'])) $input['imagem_2'] = CropImage::make('imagem_2', $this->image_config);

            $registro->update($input);

            return redirect()->route('painel.a-empresa.index')->with('success', 'Registro alterado com sucesso.');
        } catch (\Exception $e) {
            return back()->withErrors(['Erro ao alterar registro: '.$e->getMessage()]);
        }
    }
}
