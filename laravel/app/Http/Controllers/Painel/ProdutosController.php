<?php

namespace App\Http\Controllers\Painel;

use Illuminate\Http\Request;

use App\Http\Requests\ProdutosRequest;
use App\Http\Controllers\Controller;

use App\Models\Produto;
use App\Models\ProdutoCategoria;
use App\Helpers\CropImage;

class ProdutosController extends Controller
{
    private $image_config = [
        [
            'width'  => 160,
            'height' => 160,
            'path'   => 'assets/img/produtos/thumbs/'
        ],
        [
            'width'  => 465,
            'height' => 465,
            'path'   => 'assets/img/produtos/'
        ]
    ];

    private $categorias;

    public function __construct()
    {
        $this->categorias = ProdutoCategoria::ordenados()->lists('nome', 'id');
    }

    public function index(Request $request)
    {
        $filtro = $request->query('filtro');

        if (ProdutoCategoria::find($filtro)) {
            $registros = Produto::ordenados()->where('categoria_id', $filtro)->get();
        } else {
            $registros = Produto::leftJoin('produtos_categorias as cat', 'cat.id', '=', 'categoria_id')
                ->orderBy('cat.ordem', 'ASC')
                ->orderBy('cat.id', 'DESC')
                ->select('produtos.*')
                ->ordenados()->get();
        }

        return view('painel.produtos.index', compact('registros', 'filtro'))->with('categorias', $this->categorias);
    }

    public function create()
    {
        return view('painel.produtos.create')->with('categorias', $this->categorias);
    }

    public function store(ProdutosRequest $request)
    {
        try {

            $input = $request->all();
            $input['imagem'] = CropImage::make('imagem', $this->image_config);

            Produto::create($input);
            return redirect()->route('painel.produtos.index')->with('success', 'Registro adicionado com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao adicionar registro: '.$e->getMessage()]);

        }
    }

    public function edit(Produto $registro)
    {
        return view('painel.produtos.edit', compact('registro'))->with('categorias', $this->categorias);
    }

    public function update(ProdutosRequest $request, Produto $registro)
    {
        try {

            $input = array_filter($request->all(), 'strlen');
            if (isset($input['imagem'])) $input['imagem'] = CropImage::make('imagem', $this->image_config);

            $registro->update($input);
            return redirect()->route('painel.produtos.index')->with('success', 'Registro alterado com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao alterar registro: '.$e->getMessage()]);

        }
    }

    public function destroy(Produto $registro)
    {
        try {

            $registro->delete();
            return redirect()->route('painel.produtos.index')->with('success', 'Registro excluído com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao excluir registro: '.$e->getMessage()]);

        }
    }

}
