<?php

namespace App\Http\Controllers\Painel;

use Illuminate\Http\Request;

use App\Http\Requests\ObrasImagensRequest;
use App\Http\Controllers\Controller;

use App\Models\Obra;
use App\Models\ObraImagem;
use App\Helpers\CropImage;

class ObrasImagensController extends Controller
{
    private $image_config = [
        [
            'width'   => 180,
            'height'  => 180,
            'path'    => 'assets/img/obras/imagens/thumbs/'
        ],
        [
            'width'   => 800,
            'height'  => null,
            'upsize'  => true,
            'path'    => 'assets/img/obras/imagens/'
        ]
    ];

    public function index(Obra $obra)
    {
        $imagens = ObraImagem::obra($obra->id)->ordenados()->get();

        return view('painel.obras.imagens.index', compact('imagens', 'obra'));
    }

    public function show(Obra $obra, ObraImagem $imagem)
    {
        return $imagem;
    }

    public function create(Obra $obra)
    {
        return view('painel.obras.imagens.create', compact('obra'));
    }

    public function store(Obra $obra, ObrasImagensRequest $request)
    {
        try {

            $input = $request->all();
            $input['imagem'] = CropImage::make('imagem', $this->image_config);
            $input['obra_id'] = $obra->id;

            $imagem = ObraImagem::create($input);

            $view = view('painel.obras.imagens.imagem', compact('obra', 'imagem'))->render();

            return response()->json(['body' => $view]);

        } catch (\Exception $e) {

            return 'Erro ao adicionar imagem: '.$e->getMessage();

        }
    }

    public function destroy(Obra $obra, ObraImagem $imagem)
    {
        try {

            $imagem->delete();
            return redirect()->route('painel.obras.imagens.index', $obra)
                             ->with('success', 'Imagem excluída com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao excluir imagem: '.$e->getMessage()]);

        }
    }

    public function clear(Obra $obra)
    {
        try {

            $obra->imagens()->delete();
            return redirect()->route('painel.obras.imagens.index', $obra)
                             ->with('success', 'Imagens excluídas com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao excluir imagens: '.$e->getMessage()]);

        }
    }
}
