<?php

namespace App\Http\Controllers\Painel;

use Illuminate\Http\Request;

use App\Http\Requests\TecnologiaRequest;
use App\Http\Controllers\Controller;

use App\Models\Tecnologia;
use App\Helpers\CropImage;

class TecnologiaController extends Controller
{
    private $image_config = [
        'width'  => 800,
        'height' => null,
        'upsize' => true,
        'path'   => 'assets/img/tecnologia/'
    ];

    public function index()
    {
        $registro = Tecnologia::first();

        return view('painel.tecnologia.edit', compact('registro'));
    }

    public function update(TecnologiaRequest $request, Tecnologia $registro)
    {
        try {
            $input = array_filter($request->all(), 'strlen');
            if (isset($input['imagem_1'])) $input['imagem_1'] = CropImage::make('imagem_1', $this->image_config);
            if (isset($input['imagem_2'])) $input['imagem_2'] = CropImage::make('imagem_2', $this->image_config);

            $registro->update($input);

            return redirect()->route('painel.tecnologia.index')->with('success', 'Registro alterado com sucesso.');
        } catch (\Exception $e) {
            return back()->withErrors(['Erro ao alterar registro: '.$e->getMessage()]);
        }
    }
}
