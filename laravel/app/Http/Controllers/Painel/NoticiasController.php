<?php

namespace App\Http\Controllers\Painel;

use Illuminate\Http\Request;

use App\Http\Requests\NoticiasRequest;
use App\Http\Controllers\Controller;

use App\Models\Noticia;
use App\Helpers\CropImage;

class NoticiasController extends Controller
{
    private $image_config = [
        [
            'width'  => 270,
            'height' => 150,
            'path'   => 'assets/img/noticias/thumbs/'
        ],
        [
            'width'  => 100,
            'height' => 100,
            'path'   => 'assets/img/noticias/thumbs-recentes/'
        ],
        [
            'width'  => 800,
            'height' => null,
            'path'   => 'assets/img/noticias/'
        ]
    ];

    public function index()
    {
        $registros = Noticia::ordenados()->paginate(15);

        return view('painel.noticias.index', compact('registros'));
    }

    public function create()
    {
        return view('painel.noticias.create');
    }

    public function store(NoticiasRequest $request)
    {
        try {

            $input = $request->all();
            $input['capa'] = CropImage::make('capa', $this->image_config);

            Noticia::create($input);
            return redirect()->route('painel.noticias.index')->with('success', 'Registro adicionado com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao adicionar registro: '.$e->getMessage()]);

        }
    }

    public function edit(Noticia $registro)
    {
        return view('painel.noticias.edit', compact('registro'));
    }

    public function update(NoticiasRequest $request, Noticia $registro)
    {
        try {

            $input = array_filter($request->all(), 'strlen');
            if (isset($input['capa'])) $input['capa'] = CropImage::make('capa', $this->image_config);

            $registro->update($input);
            return redirect()->route('painel.noticias.index')->with('success', 'Registro alterado com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao alterar registro: '.$e->getMessage()]);

        }
    }

    public function destroy(Noticia $registro)
    {
        try {

            $registro->delete();
            return redirect()->route('painel.noticias.index')->with('success', 'Registro excluído com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao excluir registro: '.$e->getMessage()]);

        }
    }

}
