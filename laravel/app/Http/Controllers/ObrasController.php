<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;

use App\Models\Obra;

class ObrasController extends Controller
{
    public function index()
    {
        $obras = Obra::with('imagens')->ordenados()->get();

        return view('frontend.obras', compact('obras'));
    }
}
