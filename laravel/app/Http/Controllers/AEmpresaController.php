<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Models\AEmpresa;

class AEmpresaController extends Controller
{
    public function index()
    {
        $empresa = AEmpresa::first();

        return view('frontend.a-empresa', compact('empresa'));
    }
}
