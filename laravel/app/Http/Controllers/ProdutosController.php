<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Models\Produto;
use App\Models\ProdutoCategoria;

class ProdutosController extends Controller
{
    public function index($categoria = null)
    {
        if (! $categoria) $categoria = ProdutoCategoria::ordenados()->first();
        if ($categoria === null) abort('404');

        $categorias = ProdutoCategoria::ordenados()->get();
        $produtos   = $categoria->produtos()->get();

        return view('frontend.produtos', compact('produtos', 'categorias', 'categoria'));
    }
}
