<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Models\Tecnologia;

class TecnologiaController extends Controller
{
    public function index()
    {
        $tecnologia = Tecnologia::first();

        return view('frontend.tecnologia', compact('tecnologia'));
    }
}
