<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class AEmpresa extends Model
{
    protected $table = 'a_empresa';

    protected $guarded = ['id'];
}
