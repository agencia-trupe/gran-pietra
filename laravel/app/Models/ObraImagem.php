<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ObraImagem extends Model
{
    protected $table = 'obras_imagens';

    protected $guarded = ['id'];

    public function scopeObra($query, $id)
    {
        return $query->where('obra_id', $id);
    }

    public function scopeOrdenados($query)
    {
        return $query->orderBy('ordem', 'ASC')->orderBy('imagem', 'ASC');
    }
}
