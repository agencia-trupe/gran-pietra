<?php

use Illuminate\Database\Seeder;

class ContatoTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('contato')->insert([
            'whatsapp'    => '11 9.6760.5010',
            'telefone'    => '11 5575·5010',
            'email'       => 'comercial@puglieserevestimentos.com.br',
            'endereco'    => '<p>Rua Durval do Nascimento Miele, 62-A</p><p>Vila Clementino - São Paulo, SP</p><p>04026.070</p>',
            'google_maps' => '<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d5994.533775500125!2d-46.64757028569461!3d-23.60160679951726!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x94ce5a3cec7f76c5%3A0x58c3d69c48985bdf!2sR.+Durval+do+Nascimento+Miele%2C+62+-+Sa%C3%BAde%2C+S%C3%A3o+Paulo+-+SP%2C+04026-070!5e0!3m2!1spt-BR!2sbr!4v1460396977825" width="600" height="450" frameborder="0" style="border:0" allowfullscreen></iframe>',
        ]);
    }
}
