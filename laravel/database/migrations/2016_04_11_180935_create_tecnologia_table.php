<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTecnologiaTable extends Migration
{
    public function up()
    {
        Schema::create('tecnologia', function (Blueprint $table) {
            $table->increments('id');
            $table->text('texto');
            $table->string('imagem_1');
            $table->string('imagem_2');
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::drop('tecnologia');
    }
}
