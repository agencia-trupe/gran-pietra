<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAEmpresaTable extends Migration
{
    public function up()
    {
        Schema::create('a_empresa', function (Blueprint $table) {
            $table->increments('id');
            $table->text('texto');
            $table->string('imagem_1');
            $table->string('imagem_2');
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::drop('a_empresa');
    }
}
