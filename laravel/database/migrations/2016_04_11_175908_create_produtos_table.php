<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProdutosTable extends Migration
{
    public function up()
    {
        Schema::create('produtos', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('ordem')->default(0);
            $table->integer('categoria_id')->unsigned()->nullable();
            $table->string('nome');
            $table->string('imagem');
            $table->timestamps();
        });

        Schema::create('produtos_categorias', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('ordem')->default(0);
            $table->string('nome');
            $table->string('slug');
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::drop('produtos_categorias');
        Schema::drop('produtos');
    }
}
