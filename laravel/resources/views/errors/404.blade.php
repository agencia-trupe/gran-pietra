@extends('frontend.common.template')

@section('content')

    <div class="main not-found">
        <div class="center">
            <p>Página não encontrada</p>
        </div>
    </div>

@endsection
