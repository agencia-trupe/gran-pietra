@extends('painel.common.template')

@section('content')

    <legend>
        <h2>A Empresa</h2>
    </legend>

    {!! Form::model($registro, [
        'route'  => ['painel.a-empresa.update', $registro->id],
        'method' => 'patch',
        'files'  => true])
    !!}

    @include('painel.a-empresa.form', ['submitText' => 'Alterar'])

    {!! Form::close() !!}

@endsection
