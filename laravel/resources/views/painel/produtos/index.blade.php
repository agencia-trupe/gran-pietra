@extends('painel.common.template')

@section('content')

    @include('painel.common.flash')

    <legend>
        <h2>
            Produtos
            <div class="btn-group pull-right">
                <a href="{{ route('painel.produtos.create') }}" class="btn btn-success btn-sm"><span class="glyphicon glyphicon-plus" style="margin-right:10px;"></span>Adicionar Produto</a>
            </div>
        </h2>
    </legend>

    <div class="row" style="margin-bottom:20px">
        <div class="form-group col-sm-4">
            {!! Form::select('filtro', $categorias, Request::get('filtro'), ['class' => 'form-control', 'id' => 'filtro-select', 'placeholder' => 'Todas as Categorias', 'data-route' => 'painel/produtos']) !!}
        </div>
        <div class="col-sm-4" style="padding-left:0">
        <a href="{{ route('painel.produtos.categorias.index') }}" class="btn btn-primary btn-md"><span class="glyphicon glyphicon-edit" style="margin-right:10px;"></span><small>Editar Categorias</small></a>
        </div>
        @if(!$filtro)
        <div class="col-sm-4">
            <p class="alert alert-info small" style="margin-bottom: 15px; height:45px; padding: 12px 15px;">
                <span class="glyphicon glyphicon-info-sign" style="margin-right:10px;"></span>
                Selecione uma categoria para ordenar os produtos.
            </p>
        </div>
        @endif
    </div>

    @if(!count($registros))
    <div class="alert alert-warning" role="alert">Nenhum registro encontrado.</div>
    @else
    <table class="table table-striped table-bordered table-hover table-info table-sortable" data-table="produtos">
        <thead>
            <tr>
                @if($filtro)
                <th>Ordenar</th>
                @else
                <th>Categoria</th>
                @endif
                <th>Nome</th>
                <th>Imagem</th>
                <th class="no-filter"><span class="glyphicon glyphicon-cog"></span></th>
            </tr>
        </thead>

        <tbody>
        @foreach ($registros as $registro)
            <tr class="tr-row" id="{{ $registro->id }}">
                @if($filtro)
                <td>
                    <a href="#" class="btn btn-info btn-sm btn-move">
                        <span class="glyphicon glyphicon-move"></span>
                    </a>
                </td>
                @else
                <td>{!! $registro->categoria->nome or '<span class="label label-warning">Sem Categoria</span>' !!}</td>
                @endif
                <td>{{ $registro->nome }}</td>
                <td><img src="{{ asset('assets/img/produtos/thumbs/'.$registro->imagem) }}" style="width: 100%; max-width: 100px;" alt=""></td>
                <td class="crud-actions">
                    {!! Form::open([
                        'route'  => ['painel.produtos.destroy', $registro->id],
                        'method' => 'delete'
                    ]) !!}

                    <div class="btn-group btn-group-sm">
                        <a href="{{ route('painel.produtos.edit', $registro->id ) }}" class="btn btn-primary btn-sm pull-left">
                            <span class="glyphicon glyphicon-pencil" style="margin-right:10px;"></span>Editar
                        </a>

                        <button type="submit" class="btn btn-danger btn-sm btn-delete"><span class="glyphicon glyphicon-remove" style="margin-right:10px;"></span>Excluir</button>
                    </div>

                    {!! Form::close() !!}
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>


    @endif

@endsection
