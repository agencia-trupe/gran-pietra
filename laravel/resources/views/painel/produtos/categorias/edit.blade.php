@extends('painel.common.template')

@section('content')

    <legend>
        <h2><small>Produtos / Categorias /</small> Editar Categoria</h2>
    </legend>

    {!! Form::model($registro, [
        'route'  => ['painel.produtos.categorias.update', $registro->id],
        'method' => 'patch'])
    !!}

    @include('painel.produtos.categorias.form', ['submitText' => 'Alterar'])

    {!! Form::close() !!}

@endsection
