<ul class="nav navbar-nav">
    <li @if(str_is('painel.banners*', Route::currentRouteName())) class="active" @endif>
        <a href="{{ route('painel.banners.index') }}">Banners</a>
    </li>
    <li @if(str_is('painel.a-empresa*', Route::currentRouteName())) class="active" @endif>
        <a href="{{ route('painel.a-empresa.index') }}">A Empresa</a>
    </li>
    <li @if(str_is('painel.produtos*', Route::currentRouteName())) class="active" @endif>
        <a href="{{ route('painel.produtos.index') }}">Produtos</a>
    </li>
    <li @if(str_is('painel.obras*', Route::currentRouteName())) class="active" @endif>
        <a href="{{ route('painel.obras.index') }}">Obras</a>
    </li>
	<li @if(str_is('painel.clientes*', Route::currentRouteName())) class="active" @endif>
		<a href="{{ route('painel.clientes.index') }}">Clientes</a>
	</li>
	<li @if(str_is('painel.noticias*', Route::currentRouteName())) class="active" @endif>
		<a href="{{ route('painel.noticias.index') }}">Notícias</a>
	</li>
    <li @if(str_is('painel.tecnologia*', Route::currentRouteName())) class="active" @endif>
        <a href="{{ route('painel.tecnologia.index') }}">Tecnologia</a>
    </li>
    <li class="dropdown @if(str_is('painel.contato*', Route::currentRouteName())) active @endif">
        <a href="#" class="dropdown-toggle" data-toggle="dropdown">
            Contato
            @if($contatosNaoLidos >= 1)
            <span class="label label-success" style="margin-left:3px;">{{ $contatosNaoLidos }}</span>
            @endif
            <b class="caret"></b>
        </a>
        <ul class="dropdown-menu">
            <li><a href="{{ route('painel.contato.index') }}">Informações de Contato</a></li>
            <li><a href="{{ route('painel.contato.recebidos.index') }}">
                Contatos Recebidos
                @if($contatosNaoLidos >= 1)
                <span class="label label-success" style="margin-left:3px;">{{ $contatosNaoLidos }}</span>
                @endif
            </a></li>
        </ul>
    </li>
</ul>
