@extends('painel.common.template')

@section('content')

    <legend>
        <h2>Tecnologia</h2>
    </legend>

    {!! Form::model($registro, [
        'route'  => ['painel.tecnologia.update', $registro->id],
        'method' => 'patch',
        'files'  => true])
    !!}

    @include('painel.tecnologia.form', ['submitText' => 'Alterar'])

    {!! Form::close() !!}

@endsection
