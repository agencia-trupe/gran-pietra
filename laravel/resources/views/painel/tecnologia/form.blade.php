@include('painel.common.flash')

<div class="form-group">
    {!! Form::label('texto', 'Texto') !!}
    {!! Form::textarea('texto', null, ['class' => 'form-control ckeditor', 'data-editor' => 'institucional']) !!}
</div>

<div class="well form-group">
    {!! Form::label('imagem_1', 'Imagem 1') !!}
    <img src="{{ url('assets/img/tecnologia/'.$registro->imagem_1) }}" style="display:block; margin-bottom: 10px; width: 100%; max-width: 400px;">
    {!! Form::file('imagem_1', ['class' => 'form-control']) !!}
</div>

<div class="well form-group">
    {!! Form::label('imagem_2', 'Imagem 2') !!}
    <img src="{{ url('assets/img/tecnologia/'.$registro->imagem_2) }}" style="display:block; margin-bottom: 10px; width: 100%; max-width: 400px;">
    {!! Form::file('imagem_2', ['class' => 'form-control']) !!}
</div>

{!! Form::submit($submitText, ['class' => 'btn btn-success']) !!}
