@extends('frontend.common.template')

@section('content')

    <div class="main institucional">
        <div class="center">
            <div class="texto">
                {!! $tecnologia->texto !!}
            </div>

            <div class="imagens">
                <img src="{{ asset('assets/img/tecnologia/'.$tecnologia->imagem_1) }}" alt="">
                <img src="{{ asset('assets/img/tecnologia/'.$tecnologia->imagem_2) }}" alt="">
            </div>
        </div>
    </div>

@endsection
