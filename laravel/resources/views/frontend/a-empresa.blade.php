@extends('frontend.common.template')

@section('content')

    <div class="main institucional">
        <div class="center">
            <div class="texto">
                {!! $empresa->texto !!}
            </div>

            <div class="imagens">
                <img src="{{ asset('assets/img/a-empresa/'.$empresa->imagem_1) }}" alt="">
                <img src="{{ asset('assets/img/a-empresa/'.$empresa->imagem_2) }}" alt="">
            </div>
        </div>
    </div>

@endsection
