<a href="{{ route('home') }}"
@if(str_is('home', Route::currentRouteName())) class="active" @endif>
    Home
</a>
<a href="{{ route('a-empresa') }}"
@if(str_is('a-empresa', Route::currentRouteName())) class="active" @endif>
    A Empresa
</a>
<a href="{{ route('produtos') }}"
@if(str_is('produtos', Route::currentRouteName())) class="active" @endif>
    Produtos
</a>
<a href="{{ route('obras') }}"
@if(str_is('obras', Route::currentRouteName())) class="active" @endif>
    Obras
</a>
<a href="{{ route('clientes') }}"
@if(str_is('clientes', Route::currentRouteName())) class="active" @endif>
    Clientes
</a>
{{--
<a href="{{ route('noticias') }}"
@if(str_is('noticias*', Route::currentRouteName())) class="active" @endif>
    Notícias
</a>
<a href="{{ route('tecnologia') }}"
@if(str_is('tecnologia', Route::currentRouteName())) class="active" @endif>
    Tecnologia
</a>
--}}
<a href="{{ route('contato') }}"
@if(str_is('contato', Route::currentRouteName())) class="active" @endif>
    Contato
</a>
