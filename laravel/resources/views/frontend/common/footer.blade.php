    <footer>
        <div class="center">
            <div class="col">
                <nav>
                    <a href="{{ route('home') }}">Home</a>
                    <a href="{{ route('a-empresa') }}">A Empresa</a>
                    <a href="{{ route('produtos') }}">Produtos</a>
                </nav>
                <nav>
                    <a href="{{ route('obras') }}">Obras</a>
                    {{--
                    <a href="{{ route('noticias') }}">Notícias</a>
                    <a href="{{ route('tecnologia') }}">Tecnologia</a>
                    --}}
                    <a href="{{ route('contato') }}">Contato</a>
                </nav>
            </div>

            <div class="col">
                <div class="footer-logo"></div>
                <div class="footer-informacoes">
                    <p class="telefone">{{ $contato->telefone }}</p>
                    <div class="endereco">{!! $contato->endereco !!}</div>
                </div>
            </div>

            <div class="col copyright">
                <p>
                    © {{ date('Y') }} {{ config('site.name') }}
                    <br>
                    Todos os direitos reservados.
                </p>
                <p>
                    <a href="http://www.trupe.net" target="_blank">Criação de sites</a>:
                    <a href="http://www.trupe.net" target="_blank">Trupe Agência Criativa</a>
                </p>
            </div>
        </div>
    </footer>
