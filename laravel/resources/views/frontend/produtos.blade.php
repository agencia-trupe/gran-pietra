@extends('frontend.common.template')

@section('content')

    <div class="main produtos">
        <div class="center">
            <div class="produtos-categorias">
                @foreach($categorias as $cat)
                <a href="{{ route('produtos', $cat->slug) }}" @if($categoria->id === $cat->id) class="active" @endif>
                    {{ $cat->nome }}
                </a>
                @endforeach
            </div>

            <div class="produtos-lista">
                @foreach($produtos as $produto)
                <a href="{{ asset('assets/img/produtos/'.$produto->imagem) }}" class="produtos-fancybox" title="{{ $produto->nome }}" rel="produtos">
                    <div class="imagem">
                        <img src="{{ asset('assets/img/produtos/thumbs/'.$produto->imagem) }}" alt="">
                    </div>
                    <span>{{ $produto->nome }}</span>
                </a>
                @endforeach
            </div>
        </div>
    </div>

@endsection
