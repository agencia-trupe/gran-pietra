@extends('frontend.common.template')

@section('content')

    <div class="main contato">
        <div class="center">
            <div class="informacoes-wrapper">
                <div class="informacoes">
                    <p class="telefones">
                        Whatsapp: {{ $contato->whatsapp }}<br>
                        {{ $contato->telefone }}
                    </p>

                    <a href="mailto:{{ $contato->email }}">{{ $contato->email }}</a>

                    <div class="endereco">
                        {!! $contato->endereco !!}
                    </div>
                </div>

                @if($contato->google_maps)
                <div class="mapa">{!! $contato->google_maps !!}</div>
                @endif
            </div>

            <form action="" id="form-contato" method="POST">
                <h2>FALE CONOSCO</h2>
                <input type="text" name="nome" id="nome" placeholder="nome" required>
                <input type="email" name="email" id="email" placeholder="e-mail" required>
                <input type="text" name="telefone" id="telefone" placeholder="telefone">
                <textarea name="mensagem" id="mensagem" placeholder="mensagem" required></textarea>
                <input type="submit" value="ENVIAR">
                <div id="form-contato-response"></div>
            </form>
        </div>
    </div>

@endsection
