@extends('frontend.common.template')

@section('content')

    <div class="main clientes">
        <div class="center">
            <p>Para conhecer um pouco da Gran Pietra, basta conhecer alguns clientes e parceiros que confiam em nossa empresa:</p>
            <div class="clientes-lista">
                @foreach($clientes as $cliente)
                <div class="clientes-thumb">
                    <div class="imagem">
                        <img src="{{ asset('assets/img/clientes/'.$cliente->imagem) }}" alt="">
                    </div>
                    {{--<span>{{ $cliente->nome }}</span>--}}
                </div>
                @endforeach
            </div>
        </div>
    </div>

@endsection
