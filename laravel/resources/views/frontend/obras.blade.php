@extends('frontend.common.template')

@section('content')

    <div class="main obras">
        <div class="center">
            @foreach($obras as $obra)
            <a href="#" class="obras-thumb" title="{{ $obra->nome }}" data-galeria="{{ $obra->id }}">
                <div class="imagem">
                    <img src="{{ asset('assets/img/obras/'.$obra->capa) }}" alt="">
                </div>
                <span>{{ $obra->nome }}</span>
            </a>
            @endforeach
        </div>

        <div class="hidden">
            @foreach($obras as $obra)
                @foreach($obra->imagens as $imagem)
                    <a href="{{ asset('assets/img/obras/imagens/'.$imagem->imagem) }}" class="obras-fancybox" rel="galeria{{ $obra->id }}" title="{{ $obra->nome }}"></a>
                @endforeach
            @endforeach
        </div>
    </div>

@endsection
