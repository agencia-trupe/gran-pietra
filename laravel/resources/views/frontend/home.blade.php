@extends('frontend.common.template')

@section('content')

    <div class="banners">
        @foreach($banners as $banner)
        <div class="banner" style="background-image:url('{{ asset('assets/img/banners/'.$banner->imagem) }}')"></div>
        @endforeach
    </div>

    <div class="main main-home">
        <div class="center">
            <h1>
                Granitos <br>para projetos <br>residenciais e <br>corporativos
            </h1>

            {{--
            <div class="home-noticias">
                @foreach($noticias as $noticia)
                <a href="{{ route('noticias.show', $noticia->slug) }}">
                    {{ $noticia->titulo }}
                </a>
                @endforeach
            </div>
            --}}
        </div>
    </div>

@endsection
