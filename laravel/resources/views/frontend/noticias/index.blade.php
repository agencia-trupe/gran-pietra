@extends('frontend.common.template')

@section('content')

    <div class="main noticias noticias-index">
        <div class="center">
            @foreach($noticias as $noticia)
            <a href="{{ route('noticias.show', $noticia->slug) }}" class="noticia-thumb">
                <img src="{{ asset('assets/img/noticias/thumbs/'.$noticia->capa) }}" alt="">

                <div class="data">
                    <span class="dia">{{ $noticia->dia }}</span>
                    <span class="mes">{{ $noticia->mes }}</span>
                    <span class="ano">{{ $noticia->ano }}</span>
                </div>

                <h3>{{ $noticia->titulo }}</h3>
            </a>
            @endforeach
        </div>
    </div>

@endsection
