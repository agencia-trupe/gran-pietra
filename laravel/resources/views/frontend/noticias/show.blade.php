@extends('frontend.common.template')

@section('content')

    <div class="main noticias noticias-show">
        <div class="center">
            <div class="noticia-texto">
                <h1>{{ $noticia->titulo }}</h1>
                <img src="{{ asset('assets/img/noticias/'.$noticia->capa) }}" alt="">
                <div class="texto">
                    {!! $noticia->texto !!}
                </div>
                <a href="{{ route('noticias') }}" class="voltar">&laquo; VOLTAR</a>
            </div>

            <div class="noticias-recentes">
                <h4>Outras notícias recentes:</h4>
                @foreach($recentes as $recente)
                <a href="{{ route('noticias.show', $recente->slug) }}" class="noticia-thumb">
                    <div class="data">
                        <span class="dia">{{ $recente->dia }}</span>
                        <span class="mes">{{ $recente->mes }}</span>
                        <span class="ano">{{ $recente->ano }}</span>
                    </div>

                    <img src="{{ asset('assets/img/noticias/thumbs-recentes/'.$recente->capa) }}" alt="">

                    <h3>{{ $recente->titulo }}</h3>
                </a>
                @endforeach
            </div>
        </div>
    </div>

@endsection
