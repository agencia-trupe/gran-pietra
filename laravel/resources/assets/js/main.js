(function(window, document, $, undefined) {
    'use strict';

    var App = {};

    App.mobileToggle = function() {
        var $handle = $('#mobile-toggle'),
            $nav    = $('#nav-mobile');

        $handle.on('click touchstart', function(event) {
            event.preventDefault();
            $nav.slideToggle();
            $handle.toggleClass('close');
        });
    };

    App.bannersHome = function() {
        $('.banners').cycle({
            slides: '>div'
        });
    };

    App.produtosFancybox = function() {
        $('.produtos-fancybox').fancybox({
            padding: 10,
            nextSpeed: 500,
            prevSpeed: 500,
            helpers: {
                title: { type: 'inside' },
                overlay: { locked: false }
            }
        });
    };

    App.obrasFancybox = function() {
        $('.obras-fancybox').fancybox({
            padding: 10,
            nextSpeed: 500,
            prevSpeed: 500,
            helpers: {
                title: { type: 'inside' },
                overlay: { locked: false }
            }
        });

        $('.obras-thumb').click(function(e) {
            var el, id = $(this).data('galeria');
            if (id) {
                el = $('.obras-fancybox[rel=galeria' + id + ']:eq(0)');
                e.preventDefault();
                el.click();
            }
        });
    };

    App.envioContato = function(event) {
        event.preventDefault();

        var $form     = $(this),
            $response = $('#form-contato-response');

        $response.fadeOut('fast');

        $.ajax({
            type: "POST",
            url: $('base').attr('href') + '/contato',
            data: {
                nome: $('#nome').val(),
                email: $('#email').val(),
                telefone: $('#telefone').val(),
                mensagem: $('#mensagem').val(),
            },
            success: function(data) {
                $response.fadeOut().text(data.message).fadeIn('slow');
                $form[0].reset();
            },
            error: function(data) {
                $response.fadeOut().text('Preencha todos os campos corretamente.').fadeIn('slow');
            },
            dataType: 'json'
        });
    };

    App.init = function() {
        this.mobileToggle();
        this.bannersHome();
        this.produtosFancybox();
        this.obrasFancybox();
        $('#form-contato').on('submit', this.envioContato);
    };

    $(document).ready(function() {
        App.init();

        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
    });

}(window, document, jQuery));
