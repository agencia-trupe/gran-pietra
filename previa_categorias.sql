-- phpMyAdmin SQL Dump
-- version 4.5.3.1
-- http://www.phpmyadmin.net
--
-- Host: 179.188.16.118
-- Generation Time: 27-Abr-2016 às 15:36
-- Versão do servidor: 5.6.21-69.0-log
-- PHP Version: 5.6.14-0+deb8u1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `trupe1116`
--

-- --------------------------------------------------------

--
-- Estrutura da tabela `a_empresa`
--

CREATE TABLE `a_empresa` (
  `id` int(10) UNSIGNED NOT NULL,
  `texto` text COLLATE utf8_unicode_ci NOT NULL,
  `imagem_1` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `imagem_2` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Extraindo dados da tabela `a_empresa`
--

INSERT INTO `a_empresa` (`id`, `texto`, `imagem_1`, `imagem_2`, `created_at`, `updated_at`) VALUES
(1, '<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed a odio tortor. Nullam vel rutrum eros. Maecenas non porttitor arcu. Nam at purus et nisl tristique iaculis. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae.</p>\r\n\r\n<h2 class="destaque">Maecenas tortor risus, suscipit a euismod eu, posuere sed augue. Integer sed porta eros. Pellentesque in nisi ante.</h2>\r\n\r\n<p>Duis ac erat faucibus, commodo arcu vehicula, ornare urna. Sed in consectetur elit. Maecenas at metus a tortor semper volutpat. Nulla porttitor scelerisque porttitor. Ut leo orci, finibus eu ornare et, ullamcorper ac nunc. Mauris ornare malesuada vehicula. Quisque massa lacus, fringilla eget metus vel, congue lacinia justo. Donec eget suscipit tortor, quis elementum arcu. Mauris tellus leo, aliquet eu pulvinar id, pulvinar eget turpis. Integer eleifend viverra ex in porta. Quisque sodales bibendum tempus. Aenean vel mauris sed nunc posuere imperdiet vitae eu metus. Vestibulum a hendrerit neque. Praesent suscipit sed quam sit amet convallis.</p>\r\n\r\n<p>Morbi suscipit pretium neque, at dapibus libero ultrices eget. Sed et sem at turpis auctor convallis in at elit. Nulla malesuada dui ac sodales rhoncus. Maecenas et ultrices metus. Sed cursus dapibus tincidunt. Morbi porta arcu nisl, hendrerit laoreet risus egestas eget. Ut est ante, rhoncus id quam ut, dictum convallis orci. Phasellus quis lacinia eros, vel finibus ipsum. Duis euismod imperdiet elit ac molestie. Mauris at purus sapien. Duis eleifend tellus in ex eleifend, quis consequat mauris accumsan.</p>\r\n', 'img-7537_20160414143406.JPG', 'img-2587_20160414143406.JPG', NULL, '2016-04-14 14:34:07');

-- --------------------------------------------------------

--
-- Estrutura da tabela `banners`
--

CREATE TABLE `banners` (
  `id` int(10) UNSIGNED NOT NULL,
  `ordem` int(11) NOT NULL DEFAULT '0',
  `imagem` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Extraindo dados da tabela `banners`
--

INSERT INTO `banners` (`id`, `ordem`, `imagem`, `created_at`, `updated_at`) VALUES
(1, 0, 'img-7537_20160414143316.JPG', '2016-04-14 14:33:16', '2016-04-14 14:33:16'),
(2, 1, 'img-2587_20160414143322.JPG', '2016-04-14 14:33:23', '2016-04-14 14:33:23');

-- --------------------------------------------------------

--
-- Estrutura da tabela `contato`
--

CREATE TABLE `contato` (
  `id` int(10) UNSIGNED NOT NULL,
  `whatsapp` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `telefone` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `endereco` text COLLATE utf8_unicode_ci NOT NULL,
  `google_maps` text COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Extraindo dados da tabela `contato`
--

INSERT INTO `contato` (`id`, `whatsapp`, `telefone`, `email`, `endereco`, `google_maps`, `created_at`, `updated_at`) VALUES
(1, '11 9.6760.5010', '11 5575·5010', 'comercial@puglieserevestimentos.com.br', '<p>Rua Durval do Nascimento Miele, 62-A</p><p>Vila Clementino - São Paulo, SP</p><p>04026.070</p>', '<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d5994.533775500125!2d-46.64757028569461!3d-23.60160679951726!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x94ce5a3cec7f76c5%3A0x58c3d69c48985bdf!2sR.+Durval+do+Nascimento+Miele%2C+62+-+Sa%C3%BAde%2C+S%C3%A3o+Paulo+-+SP%2C+04026-070!5e0!3m2!1spt-BR!2sbr!4v1460396977825" width="600" height="450" frameborder="0" style="border:0" allowfullscreen></iframe>', NULL, NULL);

-- --------------------------------------------------------

--
-- Estrutura da tabela `contatos_recebidos`
--

CREATE TABLE `contatos_recebidos` (
  `id` int(10) UNSIGNED NOT NULL,
  `nome` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `telefone` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `mensagem` text COLLATE utf8_unicode_ci NOT NULL,
  `lido` tinyint(1) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Extraindo dados da tabela `contatos_recebidos`
--

INSERT INTO `contatos_recebidos` (`id`, `nome`, `email`, `telefone`, `mensagem`, `lido`, `created_at`, `updated_at`) VALUES
(1, 'Teste', 'teste@teste.com', '1234 5678', 'Mensagem de teste', 1, '2016-04-15 20:46:20', '2016-04-15 20:46:43');

-- --------------------------------------------------------

--
-- Estrutura da tabela `migrations`
--

CREATE TABLE `migrations` (
  `migration` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Extraindo dados da tabela `migrations`
--

INSERT INTO `migrations` (`migration`, `batch`) VALUES
('2016_02_01_000000_create_users_table', 1),
('2016_03_01_000000_create_contato_table', 1),
('2016_03_01_000000_create_contatos_recebidos_table', 1),
('2016_04_11_175211_create_banners_table', 1),
('2016_04_11_175908_create_produtos_table', 1),
('2016_04_11_180853_create_a_empresa_table', 1),
('2016_04_11_180935_create_tecnologia_table', 1),
('2016_04_11_225603_create_noticias_table', 1),
('2016_04_14_134454_create_obras_table', 1),
('2016_04_14_135929_create_obras_imagens_table', 1);

-- --------------------------------------------------------

--
-- Estrutura da tabela `noticias`
--

CREATE TABLE `noticias` (
  `id` int(10) UNSIGNED NOT NULL,
  `data` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `titulo` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `slug` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `capa` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `texto` text COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Extraindo dados da tabela `noticias`
--

INSERT INTO `noticias` (`id`, `data`, `titulo`, `slug`, `capa`, `texto`, `created_at`, `updated_at`) VALUES
(1, '2016-04-15', 'Exemplo', 'exemplo', 'img-2576_20160414144229.JPG', '<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed a odio tortor. Nullam vel rutrum eros. Maecenas non porttitor arcu. Nam at purus et nisl tristique iaculis. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Maecenas tortor risus, suscipit a euismod eu, posuere sed augue. Integer sed porta eros. Pellentesque in nisi ante. Phasellus sed rhoncus ex. Nulla sed risus id dolor fringilla elementum sed vel augue.</p>\r\n\r\n<h2 class="destaque">Duis ac erat faucibus, commodo arcu vehicula, ornare urna.</h2>\r\n\r\n<p>Sed in consectetur elit. Maecenas at metus a tortor semper volutpat. Nulla porttitor scelerisque porttitor. Ut leo orci, finibus eu ornare et, ullamcorper ac nunc. Mauris ornare malesuada vehicula. Quisque massa lacus, fringilla eget metus vel, congue lacinia justo. Donec eget suscipit tortor, quis elementum arcu. Mauris tellus leo, aliquet eu pulvinar id, pulvinar eget turpis. Integer eleifend viverra ex in porta. Quisque sodales bibendum tempus. Aenean vel mauris sed nunc posuere imperdiet vitae eu metus. Vestibulum a hendrerit neque.</p>\r\n\r\n<h2 class="subtitulo">Praesent suscipit sed quam sit amet convallis</h2>\r\n\r\n<p>Morbi suscipit pretium neque, at dapibus libero ultrices eget. Sed et sem at turpis auctor convallis in at elit. <a href="#">Nulla malesuada dui</a> ac sodales rhoncus. Maecenas et ultrices metus. Sed cursus dapibus tincidunt. Morbi porta arcu nisl, hendrerit laoreet risus egestas eget. Ut est ante, rhoncus id quam ut, dictum convallis orci. Phasellus quis lacinia eros, vel finibus ipsum. Duis euismod imperdiet elit ac molestie. Mauris at purus sapien. Duis eleifend tellus in ex eleifend, quis consequat mauris accumsan.</p>\r\n', '2016-04-14 14:42:31', '2016-04-14 14:42:31'),
(2, '2016-04-14', 'Exemplo 2', 'exemplo-2', 'dscn2240_20160414144300.JPG', '<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed a odio tortor. Nullam vel rutrum eros. Maecenas non porttitor arcu. Nam at purus et nisl tristique iaculis. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Maecenas tortor risus, suscipit a euismod eu, posuere sed augue. Integer sed porta eros. Pellentesque in nisi ante. Phasellus sed rhoncus ex. Nulla sed risus id dolor fringilla elementum sed vel augue.</p>\r\n\r\n<h2 class="destaque">Duis ac erat faucibus, commodo arcu vehicula, ornare urna.</h2>\r\n\r\n<p>Sed in consectetur elit. Maecenas at metus a tortor semper volutpat. Nulla porttitor scelerisque porttitor. Ut leo orci, finibus eu ornare et, ullamcorper ac nunc. Mauris ornare malesuada vehicula. Quisque massa lacus, fringilla eget metus vel, congue lacinia justo. Donec eget suscipit tortor, quis elementum arcu. Mauris tellus leo, aliquet eu pulvinar id, pulvinar eget turpis. Integer eleifend viverra ex in porta. Quisque sodales bibendum tempus. Aenean vel mauris sed nunc posuere imperdiet vitae eu metus. Vestibulum a hendrerit neque.</p>\r\n\r\n<h2 class="subtitulo">Praesent suscipit sed quam sit amet convallis</h2>\r\n\r\n<p>Morbi suscipit pretium neque, at dapibus libero ultrices eget. Sed et sem at turpis auctor convallis in at elit. <a href="#">Nulla malesuada dui</a> ac sodales rhoncus. Maecenas et ultrices metus. Sed cursus dapibus tincidunt. Morbi porta arcu nisl, hendrerit laoreet risus egestas eget. Ut est ante, rhoncus id quam ut, dictum convallis orci. Phasellus quis lacinia eros, vel finibus ipsum. Duis euismod imperdiet elit ac molestie. Mauris at purus sapien. Duis eleifend tellus in ex eleifend, quis consequat mauris accumsan.</p>\r\n', '2016-04-14 14:43:02', '2016-04-14 14:43:02');

-- --------------------------------------------------------

--
-- Estrutura da tabela `obras`
--

CREATE TABLE `obras` (
  `id` int(10) UNSIGNED NOT NULL,
  `ordem` int(11) NOT NULL DEFAULT '0',
  `nome` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `capa` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Extraindo dados da tabela `obras`
--

INSERT INTO `obras` (`id`, `ordem`, `nome`, `capa`, `created_at`, `updated_at`) VALUES
(3, 0, '.', 'imagens-1_20160420160054.jpg', '2016-04-20 19:00:54', '2016-04-20 19:00:54'),
(4, 0, '.', 'imagens-2_20160420160104.jpg', '2016-04-20 19:01:04', '2016-04-20 19:01:04'),
(5, 0, '.', 'imagens-3_20160420160111.jpg', '2016-04-20 19:01:11', '2016-04-20 19:01:11'),
(6, 0, '.', 'imagens-4_20160420160121.jpg', '2016-04-20 19:01:21', '2016-04-20 19:01:21'),
(7, 0, '.', 'imagens-5_20160420160127.jpg', '2016-04-20 19:01:27', '2016-04-20 19:01:27'),
(8, 0, '.', 'imagens-6_20160420160134.jpg', '2016-04-20 19:01:34', '2016-04-20 19:01:34'),
(9, 0, '.', 'imagens-7_20160420160141.jpg', '2016-04-20 19:01:41', '2016-04-20 19:01:41'),
(11, 0, '.', 'imagens-8_20160420160205.jpg', '2016-04-20 19:02:05', '2016-04-20 19:02:05'),
(12, 0, '.', 'imagens-9_20160420160217.jpg', '2016-04-20 19:02:17', '2016-04-20 19:02:17'),
(13, 0, '.', 'imagens-10_20160420160223.jpg', '2016-04-20 19:02:23', '2016-04-20 19:02:23'),
(14, 0, '.', 'imagens-11_20160420160230.jpg', '2016-04-20 19:02:30', '2016-04-20 19:02:30'),
(15, 0, '.', 'imagens-12_20160420160236.jpg', '2016-04-20 19:02:36', '2016-04-20 19:02:36'),
(16, 0, '.', 'imagens-13_20160420160247.jpg', '2016-04-20 19:02:47', '2016-04-20 19:02:47'),
(17, 0, '.', 'imagens-14_20160420160254.jpg', '2016-04-20 19:02:54', '2016-04-20 19:02:54'),
(18, 0, '.', 'imagens-15_20160420160301.jpg', '2016-04-20 19:03:01', '2016-04-20 19:03:01'),
(19, 0, '.', 'imagens-16_20160420160308.jpg', '2016-04-20 19:03:08', '2016-04-20 19:03:08'),
(20, 0, '.', 'imagens-17_20160420160316.jpg', '2016-04-20 19:03:16', '2016-04-20 19:03:16'),
(21, 0, '.', 'imagens-18_20160420160323.jpg', '2016-04-20 19:03:23', '2016-04-20 19:03:23'),
(22, 0, '.', 'imagens-19_20160420160335.jpg', '2016-04-20 19:03:35', '2016-04-20 19:03:35'),
(23, 0, '.', 'imagens-20_20160420160347.jpg', '2016-04-20 19:03:47', '2016-04-20 19:03:47'),
(24, 0, '.', 'imagens-21_20160420160354.jpg', '2016-04-20 19:03:54', '2016-04-20 19:03:54'),
(25, 0, '.', 'imagens-22_20160420160401.jpg', '2016-04-20 19:04:02', '2016-04-20 19:04:02'),
(26, 0, '.', 'imagens-23_20160420160409.jpg', '2016-04-20 19:04:09', '2016-04-20 19:04:09'),
(27, 0, '.', 'imagens-24_20160420160419.jpg', '2016-04-20 19:04:19', '2016-04-20 19:04:19'),
(28, 0, '.', 'imagens-25_20160420160425.jpg', '2016-04-20 19:04:26', '2016-04-20 19:04:26');

-- --------------------------------------------------------

--
-- Estrutura da tabela `obras_imagens`
--

CREATE TABLE `obras_imagens` (
  `id` int(10) UNSIGNED NOT NULL,
  `obra_id` int(10) UNSIGNED NOT NULL,
  `ordem` int(11) NOT NULL DEFAULT '0',
  `imagem` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Estrutura da tabela `produtos`
--

CREATE TABLE `produtos` (
  `id` int(10) UNSIGNED NOT NULL,
  `ordem` int(11) NOT NULL DEFAULT '0',
  `categoria_id` int(10) UNSIGNED DEFAULT NULL,
  `nome` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `imagem` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Extraindo dados da tabela `produtos`
--

INSERT INTO `produtos` (`id`, `ordem`, `categoria_id`, `nome`, `imagem`, `created_at`, `updated_at`) VALUES
(3, 0, NULL, 'AM Icarai', 'am-icarai-1_20160420151656.jpg', '2016-04-20 18:16:56', '2016-04-20 18:16:56'),
(4, 0, NULL, 'AM Ornamental', 'am-ornamental-1_20160420151755.jpg', '2016-04-20 18:17:55', '2016-04-20 18:17:55'),
(5, 0, NULL, 'Amendoa Sul', 'amendoa-sul-1_20160420151835.jpg', '2016-04-20 18:18:36', '2016-04-20 18:18:36'),
(6, 0, NULL, 'Arabesco', 'arabesco-1_20160420151912.jpg', '2016-04-20 18:19:12', '2016-04-20 18:19:12'),
(7, 0, NULL, 'Azul Fantástico', 'azul-fantastico-1_20160420152020.jpg', '2016-04-20 18:20:20', '2016-04-20 18:20:20'),
(8, 0, NULL, 'Basalto Fosco Flor de Camomila', 'basalto-fosco-flor-de-camomila-1_20160420152046.jpg', '2016-04-20 18:20:46', '2016-04-20 18:20:46'),
(9, 0, NULL, 'Basalto Fosco Nova P+ídula', 'basalto-fosco-nova-pidua-1_20160420152253.jpg', '2016-04-20 18:21:16', '2016-04-20 18:22:53'),
(10, 0, NULL, 'Basalto Levigado', 'basalto-levigado-1_20160420152323.jpg', '2016-04-20 18:23:23', '2016-04-20 18:23:23'),
(11, 0, NULL, 'Basalto Lustro Flor de Camomila', 'basalto-lustro-flor-de-camomila-1_20160420152350.jpg', '2016-04-20 18:23:51', '2016-04-20 18:23:51'),
(12, 0, NULL, 'Basalto Lustro Nova P+ídua', 'basalto-lustro-nova-pidua-1_20160420152424.jpg', '2016-04-20 18:24:24', '2016-04-20 18:24:24'),
(13, 0, NULL, 'Basalto Natural', 'basalto-natural-1_20160420152437.jpg', '2016-04-20 18:24:37', '2016-04-20 18:24:37'),
(14, 0, NULL, 'Basalto Preto', 'basalto-preto-01_20160420152728.jpg', '2016-04-20 18:27:28', '2016-04-20 18:27:28'),
(15, 0, NULL, 'BR Arabesco', 'br-arabesco-1_20160420152755.jpg', '2016-04-20 18:27:55', '2016-04-20 18:27:55'),
(16, 0, NULL, 'BR Ceará', 'br-ceara-2_20160420152805.jpg', '2016-04-20 18:28:05', '2016-04-20 18:28:05'),
(17, 0, NULL, 'BR Dallas', 'br-dallas-1_20160420153217.jpg', '2016-04-20 18:32:17', '2016-04-20 18:32:17'),
(18, 0, NULL, 'BR Fortaleza 1', 'br-fortaleza-1_20160420162150.jpg', '2016-04-20 18:32:52', '2016-04-20 19:21:52'),
(19, 0, NULL, 'BR Fortaleza 3', 'br-fortaleza-3_20160420153306.jpg', '2016-04-20 18:33:07', '2016-04-20 18:33:07'),
(20, 0, NULL, 'BR Itaunas', 'br-itaunas-1_20160420153324.jpg', '2016-04-20 18:33:24', '2016-04-20 18:33:24'),
(21, 0, NULL, 'BR Marfim', 'br-marfim-1_20160420153416.jpg', '2016-04-20 18:34:16', '2016-04-20 18:34:16'),
(22, 0, NULL, 'BR Paris', 'br-paris-1_20160420153426.jpg', '2016-04-20 18:34:26', '2016-04-20 18:34:26'),
(23, 0, NULL, 'BR Polo', 'br-polo-1_20160420153438.jpg', '2016-04-20 18:34:38', '2016-04-20 18:34:38'),
(24, 0, NULL, 'BR Siena', 'br-siena-1_20160420160826.jpg', '2016-04-20 18:34:58', '2016-04-20 19:08:28'),
(25, 0, NULL, 'Caf+® imperial', 'caf-imperial-1_20160420153638.jpg', '2016-04-20 18:36:38', '2016-04-20 18:36:38'),
(26, 0, NULL, 'Creme Extra', 'creme-extra-1_20160420153725.jpg', '2016-04-20 18:37:25', '2016-04-20 18:37:25'),
(27, 0, NULL, 'CZ Andorinha', 'cz-andorinha-1_20160420153759.jpg', '2016-04-20 18:37:59', '2016-04-20 18:37:59'),
(28, 0, NULL, 'CZ Andorinha 2', 'cz-andorinha-2_20160420153812.jpg', '2016-04-20 18:38:13', '2016-04-20 18:38:13'),
(29, 0, NULL, 'CZ Andorinha 3', 'cz-andorinha-3_20160420160708.jpg', '2016-04-20 18:40:23', '2016-04-20 19:07:09'),
(30, 0, NULL, 'CZ Castelo 1', 'cz-castelo-1_20160420154149.jpg', '2016-04-20 18:41:49', '2016-04-20 18:41:49'),
(31, 0, NULL, 'CZ Castelo 2', 'cz-castelo-2_20160420154205.jpg', '2016-04-20 18:42:05', '2016-04-20 18:42:05'),
(32, 0, NULL, 'CZ Castelo 3', 'cz-castelo-3_20160420154216.jpg', '2016-04-20 18:42:16', '2016-04-20 18:42:16'),
(33, 0, NULL, 'CZ Castelo 4', 'cz-castelo-4_20160420154232.jpg', '2016-04-20 18:42:32', '2016-04-20 18:43:51'),
(34, 0, NULL, 'CZ Corumbazinho 1', 'cz-corumbazinho-1_20160420154420.jpg', '2016-04-20 18:44:20', '2016-04-20 18:44:20'),
(35, 0, NULL, 'CZ Corumbazinho 2', 'cz-corumbazinho-2_20160420154433.jpg', '2016-04-20 18:44:33', '2016-04-20 18:44:33'),
(36, 0, NULL, 'CZ Corumbazinho 3', 'cz-corumbazinho-3_20160420154445.jpg', '2016-04-20 18:44:45', '2016-04-20 18:44:45'),
(37, 0, NULL, 'CZ Corumbazinho 4', 'cz-corumbazinho-4_20160420154457.jpg', '2016-04-20 18:44:57', '2016-04-20 18:44:57'),
(38, 0, NULL, 'CZ Corumbazinho 5', 'cz-corumbazinho-5_20160420154507.jpg', '2016-04-20 18:45:07', '2016-04-20 18:45:07'),
(39, 0, NULL, 'CZ Corumbazinho 6', 'cz-corumbazinho-6_20160420154725.jpg', '2016-04-20 18:47:26', '2016-04-20 18:47:26'),
(40, 0, NULL, 'CZ Corumbazinho 7', 'cz-corumbazinho-7_20160420154737.jpg', '2016-04-20 18:47:37', '2016-04-20 18:47:37'),
(41, 0, NULL, 'CZ Corumbazinho 8', 'cz-corumbazinho-8_20160420154754.jpg', '2016-04-20 18:47:54', '2016-04-20 18:47:54'),
(42, 0, NULL, 'CZ Corumbazinho 9', 'cz-corumbazinho-9_20160420154830.jpg', '2016-04-20 18:48:30', '2016-04-20 18:48:30'),
(43, 0, NULL, 'CZ Corumbazinho 10', 'cz-corumbazinho-10_20160420154841.jpg', '2016-04-20 18:48:41', '2016-04-20 18:48:41'),
(44, 0, NULL, 'DSC02617', 'dsc02617_20160420155053.jpg', '2016-04-20 18:50:54', '2016-04-20 18:50:54'),
(45, 0, NULL, 'DSC03215', 'dsc03215_20160420155108.jpg', '2016-04-20 18:51:08', '2016-04-20 18:51:08'),
(46, 0, NULL, 'DSC03866', 'dsc03866_20160420155117.jpg', '2016-04-20 18:51:17', '2016-04-20 18:51:17'),
(47, 0, NULL, 'Mármore Travertino', 'marmore-travertino-1_20160420155131.jpg', '2016-04-20 18:51:31', '2016-04-20 18:51:31'),
(48, 0, NULL, 'Marrom Guaiba', 'marrom-guaba-1_20160420155243.jpg', '2016-04-20 18:52:44', '2016-04-20 18:52:44'),
(49, 0, NULL, 'Ocre', 'ocre-1_20160420155305.jpg', '2016-04-20 18:53:05', '2016-04-20 18:53:05'),
(50, 0, NULL, 'Ouro e Prata', 'ouro-e-prata-1_20160420155316.jpg', '2016-04-20 18:53:16', '2016-04-20 18:53:16'),
(51, 0, NULL, 'Placas revestimento 12x24', 'placas-revestimento-12-x-24-1_20160420155347.jpg', '2016-04-20 18:53:47', '2016-04-20 18:53:47'),
(52, 0, NULL, 'PT São Gabriel 1', 'pt-sao-gabriel-1_20160420155437.jpg', '2016-04-20 18:54:37', '2016-04-20 18:54:37'),
(53, 0, NULL, 'PT São Gabriel 2', 'pt-sao-gabriel-2_20160420155446.jpg', '2016-04-20 18:54:46', '2016-04-20 18:54:46'),
(54, 0, NULL, 'Rosa Coral', 'rosa-coral-2_20160420155456.jpg', '2016-04-20 18:54:57', '2016-04-20 18:54:57'),
(55, 0, NULL, 'Rosa Goi+ís', 'rosa-goiis-3_20160420155511.jpg', '2016-04-20 18:55:11', '2016-04-20 18:55:11'),
(56, 0, NULL, 'Rosa Iracema', 'rosa-iracema-3_20160420160548.jpg', '2016-04-20 18:56:50', '2016-04-20 19:05:49'),
(57, 0, NULL, 'VD Ubatuba', 'vd-ubatuba-2_20160420155701.jpg', '2016-04-20 18:57:01', '2016-04-20 18:57:01'),
(58, 0, NULL, 'Verde Forest', 'verde-forest-1_20160420155712.jpg', '2016-04-20 18:57:12', '2016-04-20 18:57:12'),
(59, 0, NULL, 'VM Brasa', 'vm-brasa-1_20160420155729.jpg', '2016-04-20 18:57:29', '2016-04-20 18:57:29'),
(60, 0, NULL, 'VM  Cap+úo Bonito', 'vm-capuo-bonito-1_20160420155803.jpg', '2016-04-20 18:58:03', '2016-04-20 18:58:03'),
(61, 0, NULL, 'VM Ventura', 'vm-ventura-5_20160420155814.jpg', '2016-04-20 18:58:14', '2016-04-27 21:31:44');

-- --------------------------------------------------------

--
-- Estrutura da tabela `produtos_categorias`
--

CREATE TABLE `produtos_categorias` (
  `id` int(10) UNSIGNED NOT NULL,
  `ordem` int(11) NOT NULL DEFAULT '0',
  `nome` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `slug` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Extraindo dados da tabela `produtos_categorias`
--

INSERT INTO `produtos_categorias` (`id`, `ordem`, `nome`, `slug`, `created_at`, `updated_at`) VALUES
(1, 7, 'Fulgran Acrílico', 'fulgran-acrilico', '2016-04-27 21:28:09', '2016-04-27 21:28:09'),
(2, 6, 'Fulgran Tradicional', 'fulgran-tradicional', '2016-04-27 21:28:18', '2016-04-27 21:28:18'),
(3, 5, 'Korodur', 'korodur', '2016-04-27 21:28:30', '2016-04-27 21:28:30'),
(4, 4, 'Granilite', 'granilite', '2016-04-27 21:28:36', '2016-04-27 21:28:36'),
(5, 3, 'Marcopiso', 'marcopiso', '2016-04-27 21:28:42', '2016-04-27 21:28:42'),
(6, 2, 'Basalto', 'basalto', '2016-04-27 21:28:48', '2016-04-27 21:28:48'),
(7, 1, 'Mármores', 'marmores', '2016-04-27 21:28:56', '2016-04-27 21:28:56'),
(8, 0, 'Granitos', 'granitos', '2016-04-27 21:29:04', '2016-04-27 21:29:04');

-- --------------------------------------------------------

--
-- Estrutura da tabela `tecnologia`
--

CREATE TABLE `tecnologia` (
  `id` int(10) UNSIGNED NOT NULL,
  `texto` text COLLATE utf8_unicode_ci NOT NULL,
  `imagem_1` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `imagem_2` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Extraindo dados da tabela `tecnologia`
--

INSERT INTO `tecnologia` (`id`, `texto`, `imagem_1`, `imagem_2`, `created_at`, `updated_at`) VALUES
(1, '<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed a odio tortor. Nullam vel rutrum eros. Maecenas non porttitor arcu. Nam at purus et nisl tristique iaculis. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Maecenas tortor risus, suscipit a euismod eu, posuere sed augue. Integer sed porta eros. Pellentesque in nisi ante.</p>\r\n\r\n<h2 class="subtitulo">PHASELLUS SED RHONCUS EX</h2>\r\n\r\n<p>Duis ac erat faucibus, commodo arcu vehicula, ornare urna. Sed in consectetur elit. Maecenas at metus a tortor semper volutpat. Nulla porttitor scelerisque porttitor. Ut leo orci, finibus eu ornare et, ullamcorper ac nunc. Mauris ornare malesuada vehicula. Quisque massa lacus, fringilla eget metus vel, congue lacinia justo. Donec eget suscipit tortor, quis elementum arcu. Mauris tellus leo, aliquet eu pulvinar id, pulvinar eget turpis. Integer eleifend viverra ex in porta. Quisque sodales bibendum tempus. Aenean vel mauris sed nunc posuere imperdiet vitae eu metus. Vestibulum a hendrerit neque. Praesent suscipit sed quam sit amet convallis.</p>\r\n', 'img-2576_20160414143959.JPG', 'img-2587_20160414144000.JPG', NULL, '2016-04-14 14:41:25');

-- --------------------------------------------------------

--
-- Estrutura da tabela `users`
--

CREATE TABLE `users` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(60) COLLATE utf8_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Extraindo dados da tabela `users`
--

INSERT INTO `users` (`id`, `name`, `email`, `password`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, 'trupe', 'contato@trupe.net', '$2y$10$IrQknjleqmAMF6GGh4lJ4.9zuA3wpv0xH08sYFb7NSDvB0fUWx6i2', 'CzSL5WTtKNaDAJIWeIF07CK6RMQvteReQmwUdTFk5D9YT0PrXprFPKladOlp', NULL, '2016-04-15 20:46:36'),
(2, 'fernando', 'vendas@granpietra.com.br', '$2y$10$9ln5B.MAidDgpL6E0iUUfudrR5w1/zQtOoQXExxZ15W9fxQg9lcSS', NULL, '2016-04-20 19:25:36', '2016-04-20 19:25:36');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `a_empresa`
--
ALTER TABLE `a_empresa`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `banners`
--
ALTER TABLE `banners`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `contato`
--
ALTER TABLE `contato`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `contatos_recebidos`
--
ALTER TABLE `contatos_recebidos`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `noticias`
--
ALTER TABLE `noticias`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `obras`
--
ALTER TABLE `obras`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `obras_imagens`
--
ALTER TABLE `obras_imagens`
  ADD PRIMARY KEY (`id`),
  ADD KEY `obras_imagens_obra_id_foreign` (`obra_id`);

--
-- Indexes for table `produtos`
--
ALTER TABLE `produtos`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `produtos_categorias`
--
ALTER TABLE `produtos_categorias`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tecnologia`
--
ALTER TABLE `tecnologia`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `a_empresa`
--
ALTER TABLE `a_empresa`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `banners`
--
ALTER TABLE `banners`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `contato`
--
ALTER TABLE `contato`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `contatos_recebidos`
--
ALTER TABLE `contatos_recebidos`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `noticias`
--
ALTER TABLE `noticias`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `obras`
--
ALTER TABLE `obras`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=29;
--
-- AUTO_INCREMENT for table `obras_imagens`
--
ALTER TABLE `obras_imagens`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `produtos`
--
ALTER TABLE `produtos`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=62;
--
-- AUTO_INCREMENT for table `produtos_categorias`
--
ALTER TABLE `produtos_categorias`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;
--
-- AUTO_INCREMENT for table `tecnologia`
--
ALTER TABLE `tecnologia`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- Constraints for dumped tables
--

--
-- Limitadores para a tabela `obras_imagens`
--
ALTER TABLE `obras_imagens`
  ADD CONSTRAINT `obras_imagens_obra_id_foreign` FOREIGN KEY (`obra_id`) REFERENCES `obras` (`id`) ON DELETE CASCADE;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
